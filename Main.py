__author__ = 'Emily Charles'
from Frame import Frame
from VideoFrame import VideoFrame
from AudioFrame import AudioFrame
import plotly.plotly as py
import plotly.graph_objs as go
import PacketDatumLookup

m_Content = []
m_Frames = []
STARTFRAME_STRING = "[FRAME]"
EOFRAME_STRING = "[/FRAME]"

REALTIME = "FramesOutput-Realtime.txt"
NOT_REALTIME = "Frames-NonRealtime.txt"
GOOD = "Frames-GoodVideo.txt"
m_FileName = REALTIME

AUDIO = "audio"
VIDEO = "video"
BOTH = "BOTH"


# plotly.tools.set_credentials_file(username="DevOfAllTrades", api_key="bYmdYeCaPEcu9CjvJyrR")


def extractFrames():
    frame = Frame()
    PacketDatumLookup.initLookup()

    for i in range(0, len(m_Content)):
        line = m_Content[i]
        if STARTFRAME_STRING in line:
            if "video" in m_Content[i+1]:
                frame = VideoFrame()
            else:
                frame = AudioFrame()

        if EOFRAME_STRING not in line:
            extractDatumToFrameObj(frame, line)
        else:
            m_Frames.append(frame)

def extractDatumToFrameObj(frame, line):
    value = line.split("=", 1)

    for param in PacketDatumLookup.common_parameters:
        if param == value[0]:
            frame.getSetterFromKey(param)(value[1].strip("\n"))

    if VideoFrame == type(frame):
        for param in PacketDatumLookup.video_specific_parameters:
            if param == value[0]:
                frame.getSetterFromKey(param)(value[1].strip("\n"))
    else:
        for param in PacketDatumLookup.audio_specific_parameters:
            if param == value[0]:
                frame.getSetterFromKey(param)(value[1].strip("\n"))

def printFrames():
    for frame in m_Frames:
        print(frame)

def createPTSgraph():
    xVideoArray = []
    xAudioArray = []
    yVideoArray = []
    yAudioArray = []
    vidCount = 1;
    audCount = 1
    for packet in m_Frames:
        if(packet._codec_type == "video"):
            xVideoArray.append(vidCount)
            yVideoArray.append(packet._pts_time)
            vidCount += 1
        elif (packet._codec_type == "audio"):
            xAudioArray.append(audCount)
            yAudioArray.append(packet._pts_time)
            audCount += 1


    videoData = {'x':xVideoArray, 'y':yVideoArray}
    audioData = {'x':xAudioArray, 'y':yAudioArray}
    data = [videoData, audioData]
    layout = go.Layout(title="PTSTime " + m_FileName)
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig)

def createDurationGraph():
    xVideoArray = []
    xAudioArray = []
    yVideoArray = []
    yAudioArray = []
    vidCount = 1;
    audCount = 1
    for packet in m_Frames:
        if(packet._codec_type == "video"):
            xVideoArray.append(vidCount)
            yVideoArray.append(packet._duration_time)
            vidCount += 1
        elif (packet._codec_type == "audio"):
            xAudioArray.append(audCount)
            yAudioArray.append(packet._duration_time)
            audCount += 1

    videoData = {'x':xVideoArray, 'y':yVideoArray}
    audioData = {'x':xAudioArray, 'y':yAudioArray}
    data = [videoData, audioData]
    layout = go.Layout(title="Duration Time " + m_FileName)
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig)

def createPTSvsDurationGraph():
    xVideoArray = []
    xAudioArray = []
    yVideoArray = []
    yAudioArray = []

    for packet in m_Frames:
        if(packet._codec_type == "video"):
            xVideoArray.append(packet._pts_time)
            yVideoArray.append(packet._duration_time)
        elif (packet._codec_type == "audio"):
            xAudioArray.append(packet._pts_time)
            yAudioArray.append(packet._duration_time)


    videoData = {'x':xVideoArray, 'y':yVideoArray}
    audioData = {'x':xAudioArray, 'y':yAudioArray}
    data = [videoData, audioData]
    layout = go.Layout(title="PTS vs Duration " + m_FileName)
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig)

def createDeltaPTSTimeGraph():
    packetNums = []
    deltaPTSs = []
    average = 0;
    numPackets = 0;
    for i in range(0, len(m_Frames)):
        if i==0: continue
        else:
            if(m_Frames[i]._codec_type == "video"):
                deltaPTS = float(m_Frames[i]._pts_time) - float(m_Frames[i-1]._pts_time)
                print(deltaPTS)
                packetNums.append(i)
                deltaPTSs.append(deltaPTS)
                average += deltaPTS
                numPackets += 1

    data = [{'x':packetNums, 'y':deltaPTSs}]
    layout = go.Layout(title="Delta PTS Time " + m_FileName)
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig)

def detectGreedyFrames():
    numGreedyFrames = 0
    numPoliteFrames = 0

    for i in range(0, len(m_Frames)):
        if i==0: continue
        else:
            if(m_Frames[i]._codec_type == "video"):
                deltaPTS = float(m_Frames[i]._pts_time) - float(m_Frames[i-1]._pts_time)
                frameDuration = float(m_Frames[i]._duration_time)

                if(frameDuration > deltaPTS): numGreedyFrames += 1
                else: numPoliteFrames += 1

    print("Num greedy frames: " + str(numGreedyFrames))
    print("Num polite frames: " + str(numPoliteFrames))

def countNumFrames():
    vidCount = 0;
    audCount = 0
    for packet in m_Frames:
        if(packet._codec_type == "video"):
            vidCount += 1
        elif (packet._codec_type == "audio"):
            audCount += 1
    print("Video frames: " + str(vidCount))
    print("Audio frames: " + str(audCount))


def get_parameter_vs_count_data(param, frame_type):
    x_data = []
    y_data = []


    count = 0

    for frame in m_Frames:
        if frame_type == BOTH or frame_type == frame.getGetterFromKey(PacketDatumLookup.KEY_MEDIA_TYPE)():
            x_data.append(count)
            y_data.append(float(frame.getGetterFromKey(param)()))
            count += 1

    graph_data = {'x':x_data, 'y':y_data}
    data = [graph_data]
    layout = go.Layout(title=str(param) + " vs. Frame Count in " + m_FileName,
                       xaxis=dict(title=str(param)),
                       yaxis=dict(title="Frame Count"))
    generate_plot(data, layout)


def generate_plot(data, layout):

    fig = go.Figure(data=data, layout=layout)
    py.plot(fig)

with open("C:\\Workspace\\GSOC\\FFMPEGTester\\" + m_FileName) as f:
    m_Content = f.readlines()

extractFrames()

get_parameter_vs_count_data(PacketDatumLookup.KEY_PKT_PTS_TIME, VIDEO)


print(m_FileName)




