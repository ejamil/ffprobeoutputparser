__author__ = 'Emily Charles'

from Frame import Frame
import PacketDatumLookup

class VideoFrame(Frame):
    parentClass = Frame
    width = -1
    height = -1
    pix_fmt = ""
    sample_aspect_ratio = ""
    pict_type = ""
    coded_picture_number = -1
    display_picture_number = -1
    interlaced_frame = -1
    top_field_first = -1
    repeat_pict = -1

    def getSetterFromKey(self, key):
         if key in PacketDatumLookup.common_parameters:
             return self.parentClass.getSetterFromKey(self,key)
         else:
             if key == PacketDatumLookup.KEY_WIDTH:
                return self.set_width
             elif key == PacketDatumLookup.KEY_HEIGHT:
                return self.set_height
             elif key == PacketDatumLookup.KEY_PIX_FMT:
                return self.set_pix_fmt
             elif key == PacketDatumLookup.KEY_SAMPLE_ASPECT_RATIO:
                return self.set_sample_aspect_ratio
             elif key == PacketDatumLookup.KEY_PICT_TYPE:
                return self.set_pict_type
             elif key == PacketDatumLookup.KEY_CODED_PICTURE_NUMBER:
                return self.set_coded_picture_number
             elif key == PacketDatumLookup.KEY_DISPLAY_PICTURE_NUMBER:
                return self.set_display_picture_number
             elif key == PacketDatumLookup.KEY_INTERLACED_FRAME:
                return self.set_interlaced_frame
             elif key == PacketDatumLookup.KEY_TOP_FIELD_FIRST:
                return self.set_top_field_first
             elif key == PacketDatumLookup.KEY_REPEAT_PICT:
                return self.set_repeat_pict

    def getGetterFromKey(self, key):
         if key in PacketDatumLookup.common_parameters:
             return self.parentClass.getGetterFromKey(self,key)
         else:
             if key == PacketDatumLookup.KEY_WIDTH:
                return self.get_width
             elif key == PacketDatumLookup.KEY_HEIGHT:
                return self.get_height
             elif key == PacketDatumLookup.KEY_PIX_FMT:
                return self.get_pix_fmt
             elif key == PacketDatumLookup.KEY_SAMPLE_ASPECT_RATIO:
                return self.get_sample_aspect_ratio
             elif key == PacketDatumLookup.KEY_PICT_TYPE:
                return self.get_pict_type
             elif key == PacketDatumLookup.KEY_CODED_PICTURE_NUMBER:
                return self.get_coded_picture_number
             elif key == PacketDatumLookup.KEY_DISPLAY_PICTURE_NUMBER:
                return self.get_display_picture_number
             elif key == PacketDatumLookup.KEY_INTERLACED_FRAME:
                return self.get_interlaced_frame
             elif key == PacketDatumLookup.KEY_TOP_FIELD_FIRST:
                return self.get_top_field_first
             elif key == PacketDatumLookup.KEY_REPEAT_PICT:
                return self.get_repeat_pict

    def __str__(self):
        s = self.start_packet()
        s += str(self.parentClass)
        class_attrs = [a for a in dir(self) if not a.startswith('__') and not callable(getattr(self,a))]
        for item in class_attrs:
            s += item + " = " + str(getattr(self, item)) + " \n"

        s += self.end_packet()
        return s

    ############### SETTERS ###############
    def set_width(self, value):
        self.width = value

    def set_height(self, value):
        self.height = value

    def set_pix_fmt(self, value):
        self.pix_fmt = value

    def set_sample_aspect_ratio(self, value):
        self.sample_aspect_ratio = value

    def set_pict_type(self, value):
        self.pict_type = value

    def set_display_picture_number(self, value):
        self.display_picture_number = value

    def set_coded_picture_number(self, value):
        self.coded_picture_number = value

    def set_interlaced_frame(self, value):
        self.interlaced_frame = value

    def set_top_field_first(self, value):
        self.top_field_first = value

    def set_repeat_pict(self, value):
        self.repeat_pict = value

    ############### GETTERS ###############

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def get_pix_fmt(self):
        return self.pix_fmt

    def get_sample_aspect_ratio(self):
        return self.sample_aspect_ratio

    def get_pict_type(self):
        return self.pict_type

    def get_display_picture_number(self):
        return self.display_picture_number

    def get_coded_picture_number(self):
        return self.coded_picture_number

    def get_interlaced_frame(self):
        return self.interlaced_frame

    def get_top_field_first(self):
        return self.top_field_first

    def get_repeat_pict(self):
        return self.repeat_pict

