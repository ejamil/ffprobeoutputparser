__author__ = 'Emily Charles'

common_parameters = []
video_specific_parameters = []
audio_specific_parameters = []

KEY_MEDIA_TYPE = "media_type"
KEY_STREAM_INDEX = "stream_index"
KEY_FRAME = "key_frame"
KEY_PKT_PTS = "pkt_pts"
KEY_PKT_PTS_TIME = "pkt_pts_time"
KEY_PKT_DTS = "pkt_dts"
KEY_PKT_DTS_TIME = "pkt_dts_time"
KEY_BEST_EFFORT_TIMESTAMP = "best_effort_timestamp"
KEY_BEST_EFFORT_TIMESTAMP_TIME = "best_effort_timestamp_time"
KEY_PKT_DURATION = "pkt_duration"
KEY_PKT_DURATION_TIME = "pkt_duration_time"
KEY_PKT_POS = "pkt_pos"
KEY_PKT_SIZE = "pkt_size"

KEY_WIDTH = "width"
KEY_HEIGHT = "height"
KEY_PIX_FMT = "pix_fmt"
KEY_SAMPLE_ASPECT_RATIO = "sample_aspect_ratio"
KEY_PICT_TYPE = "pict_type"
KEY_CODED_PICTURE_NUMBER = "coded_picture_number"
KEY_DISPLAY_PICTURE_NUMBER = "display_picture_number"
KEY_INTERLACED_FRAME = "interlaced_frame"
KEY_TOP_FIELD_FIRST = "top_field_first"
KEY_REPEAT_PICT = "repeat_pict"

KEY_SAMPLE_FMT = "sample_fmt"
KEY_NB_SAMPLES = "nb_samples"
KEY_CHANNELS = "channels"
KEY_CHANNEL_LAYOUT = "channel_layout"


def initLookup():
    common_parameters.append(KEY_MEDIA_TYPE)
    common_parameters.append(KEY_STREAM_INDEX)
    common_parameters.append(KEY_FRAME)
    common_parameters.append(KEY_PKT_PTS)
    common_parameters.append(KEY_PKT_PTS_TIME)
    common_parameters.append(KEY_PKT_DTS)
    common_parameters.append(KEY_PKT_DTS_TIME)
    common_parameters.append(KEY_BEST_EFFORT_TIMESTAMP)
    common_parameters.append(KEY_BEST_EFFORT_TIMESTAMP_TIME)
    common_parameters.append(KEY_PKT_DURATION)
    common_parameters.append(KEY_PKT_DURATION_TIME)
    common_parameters.append(KEY_PKT_POS)
    common_parameters.append(KEY_PKT_SIZE)

    video_specific_parameters.append(KEY_WIDTH)
    video_specific_parameters.append(KEY_HEIGHT)
    video_specific_parameters.append(KEY_PIX_FMT)
    video_specific_parameters.append(KEY_SAMPLE_ASPECT_RATIO)
    video_specific_parameters.append(KEY_PICT_TYPE)
    video_specific_parameters.append(KEY_CODED_PICTURE_NUMBER)
    video_specific_parameters.append(KEY_DISPLAY_PICTURE_NUMBER)
    video_specific_parameters.append(KEY_INTERLACED_FRAME)
    video_specific_parameters.append(KEY_TOP_FIELD_FIRST)
    video_specific_parameters.append(KEY_REPEAT_PICT)

    audio_specific_parameters.append(KEY_SAMPLE_FMT)
    audio_specific_parameters.append(KEY_NB_SAMPLES)
    audio_specific_parameters.append(KEY_CHANNELS)
    audio_specific_parameters.append(KEY_CHANNEL_LAYOUT)